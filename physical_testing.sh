#!/bin/bash

ifconfig <interface name> down
ifconfig <interface name> hw ether <any designated MAC address>
ifconfig <interface name> up

ifconfig <interface name> down
airmon-ng check kill
iwconfig <interface name> mode monitor
ifconfig <interface name> up

airodump-ng --bssid <BSSID> --channel <channel> --write test <interface name> 
