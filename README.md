# Senior Project

This is Jordan Christian's 2020 senior project. 

Hardware Used:

- Apple MacBook Pro
- Ethernet Adapter
- 10 USB's 

Software Used:

- Kali Linux OS 
- Metasploit OS
- Virtual Box 

Important Notes:

- The file "Social Engineering Drawing" shows where USB's were placed for this experiment
- There is not a script for technical testing as most of the test was researching ports and their services, and then using Kali Linux. A script would not work for this. Commands can be found within my paper. 
- The bash scripts included can be used directly but parameters need to be changed depending on the users. Options marked with "< >" will vary for each user. 
- The file "PLEASE_READ" was what was included on each USB during the social engineering test. 
